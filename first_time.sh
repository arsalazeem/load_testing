#!/bin/bash
sudo apt install python3.8-venv
env_name="my_env"
python3 -m venv my_env
source my_env/bin/activate
