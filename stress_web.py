import time
from locust import HttpUser, task, between
import subprocess

try:
    subprocess.call("set_file_limit.sh")
except:
    pass
fetch_services = "buyer/dashboard?null&subCategoryId=:subCategoryId&type=1&minBudget=:minBudget&maxBudget=:maxBudget&sellerType=:sellerType&countryId=:countryId&sortType=:sortType&limit=10000&offset=0"
pages = {
    "first_page": "buyer/services/view",
    "second_page": "buyer/service/detail?serviceId=151",
    "login": "auth/personal/info",
    "fourth_page": "buyer/service/64"
}


class QuickstartUser(HttpUser):
    wait_time = between(1,1)

    # global_token=None
    # @task(1)
    # def task1(self):
    #     self.client.get("https://stagingapp.prowireonline.com/")
    #
    @task(1)
    def visit_first_screen(self):
        self.client.get(pages.get("first_page"))

    @task(1)
    def visit_second_screen(self):
        self.client.get(pages.get("second_page"))


    @task(1)
    def visit_login_screen(self):
        self.client.get(pages.get("login"))
    # @task(1)
    # def fetch_first_service_detail(self):
    #     self.client.get(services.get("first_service"))
    #
    # @task(1)
    # def fetch_second_service_detail(self):
    #     self.client.get(services.get("second_service"))
    #
    # @task(1)
    # def fetch_third_service_detail(self):
    #     self.client.get(services.get("third_service"))
    #
    # @task(1)
    # def fetch_fourth_service_detail(self):
    #     self.client.get(services.get("fourth_service"))

    def on_start(self):
        self.client.get("home")

    # self.client.get("https://stagingapp.prowireonline.com/find/work")
    #
    # @task(3)
    # def task3(self):
    #     self.client.get("https://stagingapp.prowireonline.com/find/work")

    # @task(3)
    # def task4(self):
    #     payload = {
    #         "email": "arsal.azeem@vizteck.com",
    #         "password": "Arsal@123#"
    #     }
    #     response = self.client.post("users/login", data=payload)
    #     token = response.json()["data"]["accessToken"]
    #     response = self.client.get("buyer/dashboard?searchText=&subCategoryId=&type=4&minBudget=&maxBudget=&sellerType=&countryId=&sortType=&limit=10&offset=0", headers={'Authorization': token})
    #
    # #http://stagingapp.prowireonline.com/explore/services

# http://stagingapp.prowireonline.com/find/work
# token=global_token
# response = self.client.get("buyer/dashboard?searchText=&subCategoryId=&type=4&minBudget=&maxBudget=&sellerType=&countryId=&sortType=&limit=10&offset=0", headers={'Authorization': token})


# response from above line
# global_token=respnse.json()["data"]["accessToken"]
# how to run this script

# locust -f main.py
# then go the the localhost:8089 it's usually 8089
